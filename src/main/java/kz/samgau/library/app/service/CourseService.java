package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Course;

public interface CourseService extends BaseService<Course, Long>{
}
