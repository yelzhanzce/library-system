package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Library;

public interface LibraryService extends BaseService<Library, Long>{
}
