package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Category;

public interface CategoryService extends BaseService<Category, Long>{
}
