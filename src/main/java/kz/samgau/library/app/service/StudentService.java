package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Student;
import kz.samgau.library.auth.dto.StudentDTO;

import java.util.List;

public interface StudentService extends BaseService<Student, Long> {
    Student createStudent(StudentDTO student);

    Student addBooksToStudent(List<Long> bookIds);

    Student findCurrentStudent();

    Student removeBookFromStudent(Long bookId);

}
