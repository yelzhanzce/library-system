package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Shelve;

public interface ShelveService extends BaseService<Shelve, Long>{
}
