package kz.samgau.library.app.service;

import kz.samgau.library.app.model.Book;

import java.util.List;

public interface BookService extends BaseService<Book, Long> {
    List<Book> findByShelve(Long shelveId);

    Double findPriceOfCurrentStudentBooks();

    Double findPriceOfStudentBooks(Long studentId);

    List<Book> findAllBooksForCurrentStudent();

    List<Book> findAllBooksByStudentId(Long id);
}
