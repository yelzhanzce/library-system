package kz.samgau.library.app.controller;

import kz.samgau.library.app.model.Book;
import kz.samgau.library.app.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public ResponseEntity<List<Book>> findAll() {
        return ResponseEntity.ok(bookService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookService.findById(id));
    }

    @GetMapping("/shelve/{id}")
    public ResponseEntity<List<Book>> findByShelve(@PathVariable("id") Long shelveId) {
        return ResponseEntity.ok(bookService.findByShelve(shelveId));
    }

    @GetMapping("/student")
    public ResponseEntity<List<Book>> findAllBooksForCurrentStudent(){
        return ResponseEntity.ok(bookService.findAllBooksForCurrentStudent());
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<List<Book>> findAllBooksByStudentId(@PathVariable Long id){
        return ResponseEntity.ok(bookService.findAllBooksByStudentId(id));
    }

    @PostMapping
    public ResponseEntity<Book> create(@RequestBody Book book) {
        return ResponseEntity.ok(bookService.saveOrUpdate(book));
    }

    @PutMapping
    public ResponseEntity<Book> update(@RequestBody Book book) {
        return ResponseEntity.ok(bookService.saveOrUpdate(book));
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        bookService.deleteById(id);
    }

    @GetMapping("/priceByCurrentStudent")
    public ResponseEntity<Double> findPriceOfCurrentStudentBooks() {
        return ResponseEntity.ok(bookService.findPriceOfCurrentStudentBooks());
    }

    @GetMapping("/priceByStudent/{id}")
    public ResponseEntity<Double> findPriceOfStudentBooks(@PathVariable Long id) {
        return ResponseEntity.ok(bookService.findPriceOfStudentBooks(id));
    }
}
