package kz.samgau.library.app.repository;

import kz.samgau.library.app.model.Shelve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShelveRepository extends JpaRepository<Shelve, Long> {
}
